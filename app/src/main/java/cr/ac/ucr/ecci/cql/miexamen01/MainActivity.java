package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import cr.ac.ucr.ecci.cql.miexamen01.Model.DeploymentScript;
import cr.ac.ucr.ecci.cql.miexamen01.Model.TableTop;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    List<TableTop> listTableTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DeploymentScript.RunScript(getApplicationContext());
        listTableTop = new TableTop().select(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.list);
        this.setList();
    }


    public void setList(){

        // Para setear el custom list adapter
        ArrayList<String> names = new ArrayList<>();
        ArrayList<Integer> images = new ArrayList<>();
        ArrayList<String> descriptions = new ArrayList<>();
        for (TableTop item:listTableTop) {
            names.add(item.getName());
            images.add(item.getImgSource());
            descriptions.add(item.getDescription().substring(0, 30) + "...");
        }

        CustomListAdapter adapter = new CustomListAdapter(this, names, images, descriptions, listTableTop);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TableTop item = (TableTop)listView.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, TableTopDetailsActivity.class);
                intent.putExtra("currentItem", item);
                startActivity(intent);
            }
        });
    }

}
