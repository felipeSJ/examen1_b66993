package cr.ac.ucr.ecci.cql.miexamen01.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class TableTop  implements Parcelable {
    private String id;
    private String name;
    private int year;
    private String publisher;
    private String country;
    private Double latitude;
    private Double longitude;
    private String description;
    private String noPlayers;
    private String ages;
    private String playingTime;
    private int imgSource;

    // Métodos constructores
    public TableTop(){}

    public  TableTop(String id, String name, int year, String publisher, String country, Double latitude, Double longitude
                    , String description, String noPlayers, String ages, String playingTime, int imgSource){
        this.setId(id);
        this.setName(name);
        this.setYear(year);
        this.setPublisher(publisher);
        this.setCountry(country);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setDescription(description);
        this.setNoPlayers(noPlayers);
        this.setAges(ages);
        this.setPlayingTime(playingTime);
        this.setImgSource(imgSource);
    }

    //Métodos get y set de los atributos
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNoPlayers() {
        return noPlayers;
    }

    public void setNoPlayers(String noPlayers) {
        this.noPlayers = noPlayers;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(String playingTime) {
        this.playingTime = playingTime;
    }

    public int getImgSource() {
        return imgSource;
    }

    public void setImgSource(int imgSource) {
        this.imgSource = imgSource;
    }



    @Override
    public String toString(){
        return this.getName();
    }

    // Metodo que permite insertar objetos TableTop a la tabla respectiva
    public void insert(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DataBaseContract.DataBaseEntry._ID, getId());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME, getName());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR, getYear());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER, getPublisher());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY, getCountry());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE, getLatitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE, getLongitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION, getDescription());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NO_PLAYERS, getNoPlayers());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES, getAges());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYING_TIME, getPlayingTime());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_IMG, getImgSource());

        db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLE_TOP, null, values);
    }

    // Método para hacer select de todas las tuplas dela tabla TableTop
    public List<TableTop> select(Context context){
        List<TableTop> list = new ArrayList<>();

        // usar la clase DataBaseHelper para realizar la operacion de leer
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DataBaseContract.DataBaseEntry.TABLE_NAME_TABLE_TOP + ";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(new TableTop(cursor.getString(0), cursor.getString(1), cursor.getInt(2)
                    , cursor.getString(3), cursor.getString(4), cursor.getDouble(5)
                    , cursor.getDouble(6), cursor.getString(7), cursor.getString(8)
                    , cursor.getString(9), cursor.getString(10), cursor.getInt(11)
            ));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    //Métodos para el Parcelable
    public TableTop(Parcel source) {
        this.setId(source.readString());
        this.setName(source.readString());
        this.setYear(source.readInt());
        this.setPublisher(source.readString());
        this.setCountry(source.readString());
        this.setLatitude(source.readDouble());
        this.setLongitude(source.readDouble());
        this.setDescription(source.readString());
        this.setNoPlayers(source.readString());
        this.setAges(source.readString());
        this.setPlayingTime(source.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.getId());
        dest.writeString(this.getName());
        dest.writeInt(this.getYear());
        dest.writeString(this.getPublisher());
        dest.writeString(this.getCountry());
        dest.writeDouble(this.getLatitude());
        dest.writeDouble(this.getLongitude());
        dest.writeString(this.getDescription());
        dest.writeString(this.getNoPlayers());
        dest.writeString(this.getAges());
        dest.writeString(this.getPlayingTime());
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }

        @Override
        public TableTop createFromParcel(Parcel source) {
            return new TableTop(source);
        }
    };

    // String array estático para utilizar en lista de detalles
    public static final String[] ATTRIBUTES =
            {
//                    "ID",
//                    "Name",
                    "Release year",
                    "Publisher name",
                    "Country name",
                    "Latitude",
                    "Longitude",
                    "Game description",
                    "Number of players",
                    "Ages",
                    "Playing time"
            };

    // Método para retornar ArrayList<String> de los detalles de una instancia de TableTop y usarlo para mostrarlos en fragmento
    public ArrayList<String> getInstanceStringArray(){
        ArrayList<String> list = new ArrayList<>();
//        list.add(this.getId());
//        list.add(this.getName());
        list.add(Integer.toString(this.getYear()));
        list.add(this.getPublisher());
        list.add(this.getCountry());
        list.add(String.valueOf(getLatitude()));
        list.add(String.valueOf(getLongitude()));
        list.add(this.getDescription());
        list.add(this.getNoPlayers());
        list.add(this.getAges());
        list.add(this.getPlayingTime());
        return list;
    }

}
