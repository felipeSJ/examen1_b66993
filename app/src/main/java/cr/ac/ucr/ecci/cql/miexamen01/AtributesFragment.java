package cr.ac.ucr.ecci.cql.miexamen01;


import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;

import cr.ac.ucr.ecci.cql.miexamen01.Model.TableTop;

public class AtributesFragment extends ListFragment {

    boolean mDualPane;
    int mCurCheckPosition;
    TableTop item;

    public AtributesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurCheckPosition = 0;
        item = ((TableTopDetailsActivity)getActivity()).getItem();
        setListAdapter( new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, TableTop.ATTRIBUTES));

        if (savedInstanceState != null) {
            // Restaura la ultima posicion seleccionada en la lista.
            mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1, TableTop.ATTRIBUTES));
        View detailsFrame = getActivity().findViewById(R.id.details);
        mDualPane = (detailsFrame != null) && (detailsFrame.getVisibility() == View.VISIBLE);

        if (mDualPane) {
            DetailsFragment details = new DetailsFragment(item.getInstanceStringArray().get(mCurCheckPosition));

            // Inicia una transaccion para inicializar el fragmento en el panel correspondiente
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.details, details);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();

            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            // Mostramos los detalles de la seleccion actual.
            showDetails(mCurCheckPosition);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Agregamos los datos a la lista a partir del arreglo estatico de TableTop
        setListAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, TableTop.ATTRIBUTES));

        View detailsFrame = getActivity().findViewById(R.id.details);
        mDualPane = (detailsFrame != null) && (detailsFrame.getVisibility() == View.VISIBLE);
        if (savedInstanceState != null) {
            // Restaura la ultima posicion seleccionada en la lista.
            mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
        }
        if (mDualPane) {
            // Si mDualPane resaltamos el item seleccionado en la lista.
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Mostramos los detalles de la seleccion actual.
            showDetails(mCurCheckPosition);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curChoice", mCurCheckPosition);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        showDetails(position);
    }

    // Muestra los detalles de una posicion actual de la lista
    void showDetails(int index) {
        mCurCheckPosition = index;
        if (mDualPane) {
            // Puede desplegar los detalles del item seleccionado en la lista.
            getListView().setItemChecked(index, true);
            // Chequea y muestra el fragmento.
            DetailsFragment details = (DetailsFragment)getFragmentManager().findFragmentById(R.id.details);

            if (details == null || details.getShownIndex() != index) {
                // Muestra el detalle de la seleccion.
                details = new DetailsFragment(item.getInstanceStringArray().get(mCurCheckPosition));

                // Inicia una transaccion para inicializar el fragmento en el panel correspondiente
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.details, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        }else {
            // Si mDualPane no permite desplegar el detalle del item seleccionado debemos
            // implementar una actividad para mostrar el detalle del item
            /*
             * Intent intent = new Intent();
             * intent.setClass(getActivity(), DetailsActivity.class);
             * intent.putExtra("index", index);
             * startActivity(intent);
             */
        }
    }
}
