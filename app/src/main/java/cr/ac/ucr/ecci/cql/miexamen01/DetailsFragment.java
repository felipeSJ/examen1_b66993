package cr.ac.ucr.ecci.cql.miexamen01;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class DetailsFragment extends Fragment {

    String detail;

    // Constructores
    public DetailsFragment(){}

    public DetailsFragment(String detail){
        this.detail = detail;
    }

    public int getShownIndex() {
        if (getArguments() == null) return 0;
        return getArguments().getInt("index", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // crea una vista de ScrollView parza mostrar los datos del item seleccionado
        ScrollView scroller = new ScrollView(getActivity());
        TextView text = new TextView(getActivity());

        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getActivity().getResources().getDisplayMetrics());
        text.setPadding(padding, padding, padding, padding);
        scroller.addView(text);
        if (container == null) {
            text.setText("HE");
            return scroller;
        }
        //Muestra el detalle en el fragmento
        text.setText(detail);
        return scroller;
    }

}
