package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import cr.ac.ucr.ecci.cql.miexamen01.Model.TableTop;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double Lat;
    private double Lon;
    private String publisherName;
    private TableTop item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obteniendo el objeto TableTop de parecelable
        item = getIntent().getExtras().getParcelable("currentItem");
        if(item != null) {
            Lat = item.getLatitude();
            Lon = item.getLongitude();
            publisherName = item.getPublisher();
        }

        setContentView(R.layout.activity_maps);
        this.setTitleOnActionBar();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Añade un marcador en la ubicación
        LatLng currentPublisher = new LatLng(this.getLat(), this.getLon());
        mMap.addMarker(new MarkerOptions().position(currentPublisher).title(this.getPubliherName()));

        // Hace zoom y acerca la cámara a la posición
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(currentPublisher)
                .zoom(15) // zoom level
                .bearing(70) // bearing // direccion de la camara
                .tilt(25) // tilt angle // inclinacion
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    // Getters de las propiedades
    public double getLat(){
        return this.Lat;
    }

    public double getLon(){
        return this.Lon;
    }

    public String getPubliherName(){
        return this.publisherName;
    }

    // Método para poner el nombre del publisher en el actionBar
    public void setTitleOnActionBar(){
        if(getActionBar() != null){
            getActionBar().setTitle(item.getPublisher());
            getActionBar().show();
        }
    }
}
