package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import cr.ac.ucr.ecci.cql.miexamen01.Model.TableTop;

public class TableTopDetailsActivity extends AppCompatActivity {

    private TableTop item;
    private Button mapButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Para recibir la instancia del objeto creado
        item = getIntent().getExtras().getParcelable("currentItem");
        this.setTitleOnActionBar();
        setContentView(R.layout.activity_table_top_deatails);
        mapButton = findViewById(R.id.mapButton);
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMapButton();
            }
        });

    }

    // Método para obtener la instancia de TableTop, usado en el fragmento de la lista
    public TableTop getItem(){
        return this.item;
    }

    // Método para poner un título adecuado en el ActionBar
    public void setTitleOnActionBar(){
        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle(item.getName());
            getSupportActionBar().show();
        }
    }

    // Define acciones del botón de mapas a la hora de hacer click
    public void setMapButton() {
        Intent intent = new Intent(TableTopDetailsActivity.this, MapsActivity.class);
        intent.putExtra("currentItem", item);
        startActivity(intent);
    }
}
