package cr.ac.ucr.ecci.cql.miexamen01;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen01.Model.TableTop;

public class CustomListAdapter extends ArrayAdapter<TableTop> {
    private final Activity context;
    private final List<TableTop> item;
    private final List<String> name;
    private final List<Integer> imgId;
    private final List<String> itemDescription;

    public CustomListAdapter(Activity context, List<String> name, List<Integer> imgId, List<String> itemDescription, List<TableTop> item){
        super(context, R.layout.custom_list, item);
        this.item = item;
        this.context = context;
        this.name = name;
        this.imgId = imgId;
        this.itemDescription = itemDescription;
    }

    // Se agregan los elementos respectivos para cada elementode la fila
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View row = inflater.inflate(R.layout.custom_list, null, true);

        TextView itemName = row.findViewById(R.id.name);
        itemName.setText(name.get(position));

        ImageView image = row.findViewById(R.id.icon);
        image.setImageResource(imgId.get(position));

        TextView description = row.findViewById(R.id.description);
        description.setText(itemDescription.get(position));

        return row;
    }

}
