package cr.ac.ucr.ecci.cql.miexamen01.Model;

import android.provider.BaseColumns;

public final class DataBaseContract {
    private DataBaseContract() {}

    public static class DataBaseEntry implements BaseColumns {
        public static final String TABLE_NAME_TABLE_TOP = "TableTop";
        // public static final String COLUMN_NAME_ID = "ID";
        public static final String COLUMN_NAME_NAME = "Name";
        public static final String COLUMN_NAME_YEAR = "Year";
        public static final String COLUMN_NAME_PUBLISHER = "Publisher";
        public static final String COLUMN_NAME_COUNTRY = "Country";
        public static final String COLUMN_NAME_LATITUDE = "Latitude";
        public static final String COLUMN_NAME_LONGITUDE = "Longitude";
        public static final String COLUMN_NAME_DESCRIPTION = "Description";
        public static final String COLUMN_NAME_NO_PLAYERS = "NoPlayers";
        public static final String COLUMN_NAME_AGES = "Ages";
        public static final String COLUMN_NAME_PLAYING_TIME = "PlayingTime";
        public static final String COLUMN_NAME_IMG = "Image";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    //Crea la tabla en la base de datos
    public static final String SQL_CREATE_COUNTRY_OCDE =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLE_TOP + " (" +
                    DataBaseEntry._ID + TEXT_TYPE + "PRIMARY KEY," +
                    //DataBaseEntry.COLUMN_NAME_ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_YEAR + INTEGER_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LATITUDE + REAL_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LONGITUDE + REAL_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NO_PLAYERS + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PLAYING_TIME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_IMG + REAL_TYPE + " )";

    // Borra la tabla de la base de datos
    public static final String SQL_DELETE_COUNTRY_OCDE = "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLE_TOP;

}


